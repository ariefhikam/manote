angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, Tugas) {

})

.controller('TugasCtrl', function($scope,$location,ionicDatePicker, Tugas) {

  var TugasCtrl = this;

  //TugasCtrl.tugas = [];

  var ipObj1 = {
    callback: function (val) {  //Mandatory
      var d = new Date(val);

      TugasCtrl.date = d.getFullYear()  + "-" + (d.getMonth()+1) + "-" + d.getDate();
    },
    from: new Date(), //Optional
    to: new Date(2016, 10, 30), //Optional
    inputDate: new Date(),      //Optional
    mondayFirst: true,          //Optional
    closeOnSelect: true,       //Optional
    templateType: 'popup'       //Optional
  };

  $scope.$on('$ionicView.enter', function(e) {
    TugasCtrl.refresh();
  });

  TugasCtrl.refresh = function(){
    console.log('refresh');
    TugasCtrl.tugas = Tugas.all();
  }

  TugasCtrl.simpan = function(){

    Tugas.insert(TugasCtrl.nama,TugasCtrl.desc,TugasCtrl.date);
    TugasCtrl.nama = '';
    TugasCtrl.desc = '';
    TugasCtrl.date = '';

    $location.path("#/tab/dash");
  }

  TugasCtrl.openDatePicker = function(){
    ionicDatePicker.openDatePicker(ipObj1);
  };

  TugasCtrl.refresh();
  //console.log(this);
})

.controller('TugasDetailCtrl', function($scope, $stateParams, $location, Tugas) {

  var TugasCtrl = this;
  
  TugasCtrl.tugas = Tugas.get($stateParams.tugasId);

  TugasCtrl.destroy = function(id){
    console.log('delete');
    Tugas.delete(id);

    $location.path("#/tab/dash");
  }

  //console.log(TugasCtrl.tugas);
})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
  //console.log(',asuk');
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});


