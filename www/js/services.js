angular.module('starter.services', ['ngCordova'])

.factory('Chats', function($ionicPlatform, $cordovaSQLite) {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})

.factory('Tugas', function($ionicPlatform, $cordovaSQLite) {

  return {
    all:function(){
      var query = "SELECT * FROM tugas ORDER BY date DESC";
      var table = []
      $cordovaSQLite.execute(db,query).then(function(result) {
        if(result.rows.length > 0) {
          for(var i = 0; i < result.rows.length; i++) {
            table.push({
              id: result.rows.item(i).id, 
              name: result.rows.item(i).name,
              desc: result.rows.item(i).description,
              date: result.rows.item(i).date
            });
          }
        } else {
          console.log("NO ROWS EXIST");
        }
      }, function(error) {
        console.error(error);
      });

      return table;
    },

    insert:function(name, description, date) {
      var query = "INSERT INTO tugas (name, description, date,created_at) VALUES (?,?,?,?)";
      $cordovaSQLite.execute(db,query,[name,description,date,new Date()]).then(function(result) {
        console.log(query);
        return result.insertId;
      }, function(error) {
        console.error(error);
      });
    },

    get: function(id){
      var query = "SELECT * FROM tugas WHERE id = ?";
      var table = []
      $cordovaSQLite.execute(db,query,[id]).then(function(result) {
        if(result.rows.length > 0) {
          //for(var i = 0; i < result.rows.length; i++) {
            table.id = result.rows.item(0).id;
            table.name = result.rows.item(0).name;
            table.desc = result.rows.item(0).description;
            table.date = result.rows.item(0).date;
            table.created_at = result.rows.item(0).created_at;
            // table = [
            //   id: result.rows.item(0).id, 
            //   name: result.rows.item(0).name,
            //   desc: result.rows.item(0).description,
            //   date: result.rows.item(0).date
            // ];
          //}
        } else {
          console.log("NO ROWS EXIST");
        }
      }, function(error) {
        console.error(error);
      });
      return table;
    },

    delete: function(id){
      var query = "DELETE FROM tugas WHERE id = ?";
      var table = []
      $cordovaSQLite.execute(db,query,[id]).then(function(result) {
       
      }, function(error) {
        console.error(error);
      });
      return 1;
    }
  }
});