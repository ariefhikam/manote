// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var db = null;
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngCordova','ionic-datepicker'])

.run(function($ionicPlatform, $cordovaSQLite) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    if (window.cordova) {
      db = $cordovaSQLite.openDB({ name: "db.db" }); //device
    }else{
      db = window.openDatabase("db.db", '1', 'db', 1024 * 1024 * 100); // browser
    }
    $cordovaSQLite.execute(db,"CREATE TABLE IF NOT EXISTS `jadwal` ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `nama` TEXT, `jam` TEXT, `dosen` TEXT, `hari` TEXT, `created_at` TEXT )");
    $cordovaSQLite.execute(db,"CREATE TABLE IF NOT EXISTS `tugas` ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `description` TEXT, `date` TEXT, `critical` TEXT, `created_at` INTEGER, `jadwal_id` INTEGER )");
    
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  //lh Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $ionicConfigProvider.views.maxCache(0);
  $stateProvider



  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.home', {
    url: '/home',
    views: {
      'tab-dash': {
        templateUrl: 'templates/home.html',
        controller: 'TugasCtrl'
      }
    }
  })

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        //controller: 'TugasCtrl'
      }
    }
  })
  .state('tab.tugas-form', {
      url: '/tugas/tambah',
      views: {
        'tab-dash': {
          templateUrl: 'templates/tugas-form.html',
          controller: 'TugasCtrl'
        }
      }
    })
  .state('tab.tugas-detail', {
      url: '/tugas/:tugasId',
      views: {
        'tab-dash': {
          templateUrl: 'templates/tugas-detail.html',
          controller: 'TugasDetailCtrl'
        }
      }
    })
  
  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.jadwal-detail', {
      url: '/jadwal/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/jadwal-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })
    
  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/home');

});

function ContentController($scope, $ionicSideMenuDelegate) {
  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };
}